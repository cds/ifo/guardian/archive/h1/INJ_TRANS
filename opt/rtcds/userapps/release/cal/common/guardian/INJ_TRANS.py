import os
import awg
from gpstime import gpsnow
import numpy as np
from guardian import GuardState
from guardian import GuardStateDecorator
from injtools import close_all_streams

# File location: /opt/rtcds/userapps/trunk/cal/common/guardian
# Ryan Crouch, LHO 08/2022 Pre-O4 epoch

nominal = 'NO_INJ'

IFO = os.getenv("IFO")

# Log file that will contain injection parameters
# Split by each type? **Probably don't need this**
LOG_DIR = " "
LOG_FILE = os.path.join(LOG_DIR, "{:.0f}".format(gpsnow()) + ".txt")
log_info = []

# Global injection parameters, might need to edit
RAMP_TIME = 5  # seconds
DURATION = 30  # seconds, was 90
TIME_BETWEEN_INJECTIONS = 5  # seconds

# name of channel to inject transient signals
EXC_FILTER = "CAL-INJ_TRANSIENT"
EXC_CHANNEL = EXC_FILTER + "_EXC"


# Abort
def kill_all_streams():
    """ Create a GuardStateDecorator that aborts all streams and resets the
    HardwareInjection.stream class attribute to None. This decorator will also
    set the gain of the filterbank to 0.

    Parameters
    ----------
    inj_type: dict
        A list of HardwareInjection instances.

    Returns
    ----------
    kill_all_streams_decorator: GuardStateDecorator
        A GuardStateDecorator that aborts all streams.
    """

    class ABORT_INJ(GuardStateDecorator, GuardState):
        """ Decorator for GuardState that will aborts all streams and then
        resets them to None. This decorator will also set the gain of the
        filterbank to 0.
        """
        # goto = True
        # Put it outside of the main branch
        index = 30

        def pre_exec(self):
            """ Do this before entering the GuardState.
            """

            # ramp gain to 0
            ezca.ramp_gain(
                EXC_FILTER,
                0.0,
                ramp_time=RAMP_TIME,
                wait=True,
            )

            # close all streams
            try:
                close_all_streams(inj_type)
            except:
                log_traceback()
                return "FAILURE_TO_KILL_STREAM"

    return CLEANUP


# Creating a nested dictionary to store the 3 injection types, as of 8/15 I'm just putting in random values
inj_type = {}

inj_type_dict = {
    'DETCHAR': {'freq_low': 10, 'freq_high': 100, 'gain': 40000, 'injection': None},
    'LOWLATENCY': {'freq_low': 10, 'freq_high': 100, 'gain': 40000, 'injection': None},
    'CALIBRATION': {'freq_low': 10, 'freq_high': 100, 'gain': 40000, 'injection': None}
}


# Create an `awg.Excitation` object with the desired injection parameters.
def initialize_exc(inj_type, channel):
    channel = IFO + ':' + channel

    if inj_type == 'DETCHAR':
        a = awg.UniformNoise(channel, ampl=0.001, freq1=.1, freq2=2000)
        return a

    elif inj_type == 'CALIBRATION':
        a = awg.UniformNoise(channel, ampl=0.001, freq1=.1, freq2=2000)
        return a

    elif inj_type == 'LOWLATENCY':
        a = awg.UniformNoise(channel, ampl=0.001, freq1=.1, freq2=2000)
        return a


# Main parent class, GuardianState INIT, last state to work on
class INIT(GuardState):
    def main(self):
        return True


# State to stop any and all injections, the down state essentially. Nominal state
class NO_INJ(GuardState):
    index = 2

    def run(self):
        return True


# Wrapper for the cleanup state so that cleanup states can be generated for each injection
def gen_CLEANUP():
    class CLEANUP(GuardState):
        request = False

        def main(self):
            log_info = []
            # If coming from a higher state (cancelled injection), current injection should be cleared
            # Stop injections
            for name, injection in inj_type.items():
                log("Stopping " + name)
                if injection.started:
                    injection.stop(ramptime=RAMP_TIME)

            # Clear injections
            for name, injection in inj_type.items():
                log("Clearing " + name)
                injection.clear()

            # Turn off gain to filter banks
            ezca.get_LIGOFilter(EXC_FILTER).ramp_gain(0, ramp_time=0)
            ezca.get_LIGOFilter(EXC_FILTER).switch_off('OUTPUT')

        def run(self):
            return True

    return CLEANUP


# Making a wrapper to use for all the states that do the injections
def gen_PERFORM_INJ(inj_type):
    # Non requestable state to do the injection, it takes parameters that will differentiate it for detchar, ll, lowlat
    class PERFORM_INJ(GuardState):
        request = False

        def main(self):
            freq_low = inj_type_dict[inj_type]['freq_low']
            freq_high = inj_type_dict[inj_type]['freq_high']
            gain = inj_type_dict[inj_type]['gain']
            # Get bandpass filter
            # 08/12, commented out the below so it doesn't turn on input&output for testing
            # NEEDS TO BE UNCOMMENTED AT SOME POINT, WHEN THE NODE IS FINISHED
            # ezca.get_LIGOFilter(EXC_FILTER).only_on('INPUT', 'OUTPUT', 'DECIMATION', 'FM1')
            ezca.get_LIGOFilter(EXC_FILTER).only_on('DECIMATION', 'FM1')
            ezca.get_LIGOFilter(EXC_FILTER).ramp_gain(1, ramp_time=0)  # sets gain to 1 from 0
            # Create an `awg.Excitation` object for this channel
            inj_type_dict[inj_type]['injection'] = initialize_exc(inj_type, channel=EXC_CHANNEL)
            inj_type_dict[inj_type]['injection'].set_gain(gain)
            log(f"Starting {freq_low}-{freq_high}Hz injection to {EXC_CHANNEL} channel")
            inj_type_dict[inj_type]['injection'].start(ramptime=RAMP_TIME)
            # inj_type['{} low_freq'.format()].start(ramptime=RAMP_TIME) -- old style
            # START THE INJECTION
            self.t_start = gpsnow()
            # Wait for the duration of the injection so that injections do not overlap in time
            self.timer['Injection duration'] = DURATION
            self.timer['buffer time'] = 0
            self.clearflag = False

        def run(self):
            if self.timer['Injection duration'] and not self.clearflag:
                # Stop the injection because its full duration has elapsed
                log("Injection finished")
                # STOP THE INJECTION
                inj_type_dict[inj_type]['injection'].stop(ramptime=RAMP_TIME)
                inj_type_dict[inj_type]['injection'].clear()
                self.t_end = gpsnow()
                log(log_info)
                self.clearflag = True
            if self.timer['buffer time'] and self.clearflag:
                return True
            return False

    return PERFORM_INJ


def read_waveform(waveform_path, ftype="ascii"):
    """ Reads a waveform file. Only single-column ASCII files are
    supported for reading.

    Parameters
    ----------
    waveform_path: str
        Path to the waveform file.
    ftype: str
        Selects what method to use. Currently must be a string set to "ascii".

    Retuns
    ----------
    waveform: numpy.array
        Returns the time series as a numpy array.
    """

    waveform_path = '/opt/rtcds/userapps/trunk/cal/common/guardian/StarWars3.wav'
    # single-column ASCII file reading
    if ftype == "ascii":
        # read single-column ASCII file with time series
        waveform = np.loadtxt(waveform_path, usecols=1, dtype='float')

    return waveform

# Combine with above state, TODO
class read_files(GuardState):
    index = 15
    request = False
    """
    A state to read in the waveform file(s)
    """


class load_files(GuardState):
    index = 16
    request = False
    """
    A state to load in the files
    """


class prep_files(GuardState):
    index = 17
    request = False
    """
    State to prepare the files, reformatting? Scaling?
    """


# Requestable state to perform the detchar injection
class DETCHAR_COMPLETE(GuardState):
    index = 5

    def run(self):
        return True


# Requestable state to perform the lowlatency injection
class LOWLATENCY_COMPLETE(GuardState):
    index = 8

    def run(self):
        return True


# Requestable state to perform the calibration injection
class CALIBRATION_COMPLETE(GuardState):
    index = 11

    def run(self):
        return True


# The second branch, INJ_ALL

# State to go through and run all three injections, only requestable state in branch
class INJ_ALL(GuardState):
    index = 20

    def main(self):
        log("Starting all injections")
        notify("Starting all injections")

        return True


# have a low, high, gain, type arg that will be lists made from the dict
"""
Using the setup class generators to generate classes that will all act the same but be passed different 
parameters. I'm also adding indexes to the generated classes using .index else they will be given random neg indexes
"""
# Generated class that will perform the DETCHAR injection, single injection
INJ_DETCHAR = gen_PERFORM_INJ('DETCHAR')
INJ_DETCHAR.index = 3
# Generated class that will perform the lowlatency injection, single injection
INJ_LL = gen_PERFORM_INJ('LOWLATENCY')
INJ_LL.index = 6
# Generated class that will perform the calibration injection, single injection
INJ_CAL = gen_PERFORM_INJ('CALIBRATION')
INJ_CAL.index = 9
# Generated class that will perform the DETCHAR injection in the all branch
DET_INJ_ALL = gen_PERFORM_INJ('DETCHAR')
DET_INJ_ALL.index = 12
# Generated class that will perform the lowlatency injection in the all branch
LOWLAT_INJ_ALL = gen_PERFORM_INJ('LOWLATENCY')
LOWLAT_INJ_ALL.index = 13
# Generated class that will perform the calibration injection in the all branch
CAL_INJ_ALL = gen_PERFORM_INJ('CALIBRATION')
CAL_INJ_ALL.index = 14

# Cleanup states GOTO cleanup that will be used to jump to NO_INJ for canceling running injection, high index so its
# outside the main branch
CLEANUP = gen_CLEANUP()
CLEANUP.goto = True
CLEANUP.index = 110

# Cleanups for the three single injections
CAL_CLEANUP = gen_CLEANUP()
CAL_CLEANUP.index = 10
LL_CLEANUP = gen_CLEANUP()
LL_CLEANUP.index = 7
DETCHAR_CLEANUP = gen_CLEANUP()
DETCHAR_CLEANUP.index = 4

# Cleanup for the all branch
ALL_CLEANUP = gen_CLEANUP()
ALL_CLEANUP.index = 21

# ABORT_INJ = kill_all_streams()
# ABORT_INJ.goto = True
# This was giving me index issues, FIX LATER TODO

######################
# Two main branches, for doing injections individually or doing all 3 of them sequentially
edges = [
    ('CLEANUP', 'NO_INJ'),
    ('NO_INJ', 'INJ_DETCHAR'),
    ('INJ_DETCHAR', 'DETCHAR_CLEANUP'),
    ('DETCHAR_CLEANUP', 'DETCHAR_COMPLETE'),
    ('DETCHAR_COMPLETE', 'NO_INJ'),
    ('NO_INJ', 'INJ_LL'),
    ('INJ_LL', 'LL_CLEANUP'),
    ('LL_CLEANUP', 'LOWLATENCY_COMPLETE'),
    ('LOWLATENCY_COMPLETE', 'NO_INJ'),
    ('NO_INJ', 'INJ_CAL'),
    ('INJ_CAL', 'CAL_CLEANUP'),
    ('CAL_CLEANUP', 'CALIBRATION_COMPLETE'),
    ('CALIBRATION_COMPLETE', 'NO_INJ'),
    ('NO_INJ', 'DET_INJ_ALL'),  # Second branch for doing all of them
    ('DET_INJ_ALL', 'LOWLAT_INJ_ALL'),
    ('LOWLAT_INJ_ALL', 'CAL_INJ_ALL'),
    ('CAL_INJ_ALL', 'ALL_CLEANUP'),
    ('ALL_CLEANUP', 'INJ_ALL'),
    ('INJ_ALL', 'NO_INJ'),
   # ('ABORT_INJ', 'CLEANUP')
]
